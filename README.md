Player activity tracker + ignoreNicknames
=============

IITC標準のPlayer activity trackerに、特定のplayerについての表示を行わないようにする機能を追加しました。

導入方法
--------
ブラウザにてファイルをRaw形式で取得すれば Plugin Install の画面が表示されます。
  URL: <https://gitlab.com/ynakata/iitc-player-activity-tracker-ignorenickname/-/raw/main/player-activity-tracker+ignoreNicknames.user.js?ref_type=heads>

標準の Player Activity Trackerは未使用にするかアンインストールしてください(置き換えになります：同時には使用できません)


非表示対象を増やす
--------
プラグイン定義末尾の window.plugin.playerTracker.ignoreNicknames の定義の部分に、非表示にしたいユーザー名を追加してください。
(初期状態では 'NIASection14' だけが指定されています)